from django.urls import path
from receipts.views import (
    receipt_list,
    create_receipt,
    receipts_accounts,
    receipts_categories,
    create_category,
    create_account,
)


urlpatterns = [
    path("", receipt_list, name="home"),
    path("create/", create_receipt, name="create_receipt"),
    path("accounts/", receipts_accounts, name="receipts_accounts"),
    path("categories/", receipts_categories, name="receipts_categories"),
    path("categories/create/", create_category, name="create_category"),
    path("accounts/create/", create_account, name="create_account"),
]
