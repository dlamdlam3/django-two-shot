from django.contrib import admin
from receipts.models import Account, ExpenseCategory, Receipt


# Register your models here.


@admin.register(ExpenseCategory)
class ExpensesAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "owner",
        "id",
    )


@admin.register(Account)
class AccounAdmin(admin.ModelAdmin):
    list_display = (
        "name",
        "number",
        "owner",
        "id",
    )


@admin.register(Receipt)
class ReceiptAdmin(admin.ModelAdmin):
    list_display = (
        "vendor",
        "total",
        "tax",
        "date",
        "purchaser",
        "category",
        "account",
        "id",
    )
